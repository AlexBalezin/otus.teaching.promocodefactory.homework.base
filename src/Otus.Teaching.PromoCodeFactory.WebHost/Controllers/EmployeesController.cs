﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// Создать сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateEmployeeAsync(EmployeeCreateOrUpdateRequest employeeCreateRequest)
        {
            var employees = await _employeeRepository.GetAllAsync();
            if (employees.Any(e => e.Email == employeeCreateRequest.Email))
            {
                return BadRequest("Сотрудник с указанным email уже сществует");
            }

            var employee = new Employee
            {
                Id = Guid.NewGuid(),
                FirstName = employeeCreateRequest.FirstName,
                LastName = employeeCreateRequest.LastName,
                Email = employeeCreateRequest.Email,
                Roles = employeeCreateRequest.Roles,
                AppliedPromocodesCount = employeeCreateRequest.AppliedPromocodesCount
            };

            await _employeeRepository.CreateAsync(employee);

            return Ok();
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteEmployeeAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            if (employee == null)
            {
                return BadRequest("Сотрудник с указанным Id не существует");
            }

            await _employeeRepository.DeleteByIdAsync(id);

            return Ok();
        }

        /// <summary>
        /// Обновить данные сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateEmployeeAsync(EmployeeCreateOrUpdateRequest employeeUpdateRequest, Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            if (employee.Id == null)
            {
                return BadRequest("Сотрудник с указанным Id не существует");
            }

            employee.FirstName = employeeUpdateRequest.FirstName;
            employee.LastName = employeeUpdateRequest.LastName;
            employee.Email = employeeUpdateRequest.Email;
            employee.Roles = employeeUpdateRequest.Roles;
            employee.AppliedPromocodesCount = employeeUpdateRequest.AppliedPromocodesCount;

            return Ok();
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }
    }
}